CURRENT_GIT_VERSION := $(shell git describe --abbrev=8 --dirty --always --tags)

default: dist

dist:
	tar cfvzp hosting-scripte-$(CURRENT_GIT_VERSION).tar.gz -C src .

clean:
	rm -f hosting-scripte-*.tar.gz
