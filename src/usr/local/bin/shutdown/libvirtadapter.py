import libvirt
import time
import logging

import errorcodes

class LibvirtAdapter:

    MAX_WAIT_FOR_SHUTDOWN = 60
    MAX_WAIT_FOR_START = 120
    WAIT_TIME_INCREMENT = 5

    def __init__(self):
        self.connection = self.createLibVirtConnection()

    def createLibVirtConnection(self):
        conn = libvirt.open('qemu:///system')
        if conn is None:
            logging.error('Failed to open connection to qemu:///system')
            exit(errorcodes.EXIT_HOST_CONNECTION_ERROR)

        return conn


# Adapter Methods

    def getAllDomains(self):
        return self.connection.listAllDomains(0)

    def getAllDomainNames(self):
        allDomainNames = []

        allDomains = self.getAllDomains()

        for domain in allDomains:
            allDomainNames.append(self.getDomainName(domain))

        return allDomainNames


    def lookupByName(self, domainName):
        return self.connection.lookupByName(domainName)

    def getDomainInfo(self, domain):
        return domain.info()

    def isDomainActive(self, domain):
        return domain.isActive()

    def getDomainName(self, domain):
        return domain.name()

    def getDomainRawXML(self, domain):
        return domain.XMLDesc(0)

    def shutdownDomain(self, domain):
        domainName = self.getDomainName(domain)
        logging.info("Now trying to shutdown: " + domainName)

        shutdown = domain.shutdown()

        waitTime = 0

        while self.isDomainActive(domain):
            time.sleep(self.WAIT_TIME_INCREMENT)
            waitTime += self.WAIT_TIME_INCREMENT

            logging.info("Now waiting {0} s for shutdown of {1}".format(waitTime, domainName))

            if waitTime >= self.MAX_WAIT_FOR_SHUTDOWN:
                logging.error("{0} did not shutdown in time. It took longer than the configured maximum of {1}".format(domainName, self.MAX_WAIT_FOR_SHUTDOWN))
                logging.error("Shutdown of {0} will be stopped".format(domainName))

                raise ShutdownError("{0} did not shutdown in time".format(domainName))

        logging.info("{0} has been successfully shutdown".format(domainName))
        return shutdown

    def startDomain(self, domain):
        domainName = self.getDomainName(domain)

        logging.info("Trying to restart: {0}".format(domainName))

        start = domain.create()

        waitTime = 0

        while not self.isDomainActive(domain):
            time.sleep(self.WAIT_TIME_INCREMENT)
            waitTime += self.WAIT_TIME_INCREMENT

            logging.info("Now waiting {0} s for startup of {1}".format(waitTime, domainName))

            if waitTime >= self.MAX_WAIT_FOR_START:
                logging.error("{0} did not start in time.".format(domainName))
                logging.error("{0} startup will be stopped".format(domainName))

                raise StartupError("{0} did not startup in time".format(domainName))

        logging.info("{0} has been successfully started".format(domainName))


class ShutdownError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class StartupError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)