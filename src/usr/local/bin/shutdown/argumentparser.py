import sys, getopt
import ConfigParser

import errorcodes

class ArgumentParser:

    def __init__(self):
        self.vmListFilePath = ""
        self.configFilePath = ""

    def parseArguments(self, argv):

        try:
            opts, args = getopt.getopt(argv, "hic:", ["help", "vmfile=", "configfile="])
        except getopt.GetoptError:
            print("main.py --vmfile <fileWithVmList> --configfile <ConfigurationFile>")
            sys.exit(errorcodes.EXIT_CONFIG_ERROR)
        for opt, arg in opts:
            if opt in ("-h", "--help", ""):
                print("main.py --vmfile <fileWithVmList> --configfile <ConfigurationFile>")
                sys.exit()
            elif opt in ("-i", "--vmfile"):
                self.vmListFilePath = arg
            elif opt in ("-c", "--configfile"):
                self.configFilePath = arg

    def getVmList(self):
        return self.getListFromFile(self.vmListFilePath)

    def getListFromFile(self, filepath):
        with open(filepath) as file:
            # remove whitespace characters like '\n' at the end of each line
            content = file.read().splitlines()

        return content

    def getTargetDirectory(self):
        return self.readBackupConfigurationByKey("targetDirectory")

    def getRebootAfterBackup(self):
        return self.readBackupConfigurationByKey("rebootAfterBackup")

    def readBackupConfigurationByKey(self, key):
        return self.readConfigurationBySectionAndKey("backup", key)

    def readConfigurationBySectionAndKey(self, section, key):
        config = ConfigParser.ConfigParser()
        config.readfp(open(self.configFilePath))

        # read configuration entries for a given key in the given section
        result = config.get(section, key)

        return result

