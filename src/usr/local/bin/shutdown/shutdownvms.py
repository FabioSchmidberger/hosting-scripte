import logging

import libvirtadapter
import errorcodes

class ShutdownVms ():

    def __init__(self):
        self.libvirt = libvirtadapter.LibvirtAdapter()
        self.domainsThatHaveBeenShutdown = []


    def checkIfListOfDomainNamesExists (self, domainNameList):
        allAvailableDomainNames = self.libvirt.getAllDomainNames()

        logging.info("All available Domains are: [ {0} ]".format(allAvailableDomainNames))

        for domainName in domainNameList:
            if domainName not in allAvailableDomainNames:
                logging.error("Script will be aborted because {0} is no available Domain".format(domainName))
                exit(errorcodes.EXIT_CONFIG_ERROR)


    def shutdownDomains (self, domainNameList):

        logging.info("Now trying to shutdown the following domains: [ {0} ]".format(domainNameList))

        for domainName in domainNameList:
            domain = self.libvirt.lookupByName(domainName)

            if self.libvirt.isDomainActive(domain):

                try:
                    self.libvirt.shutdownDomain(domain)
                    self.domainsThatHaveBeenShutdown.append(domain)

                except libvirtadapter.ShutdownError:
                    logging.info("Failed to shutdown {0}".format(domainName))
                    self.restartAllShutdownDomains()
                    # abort shutdown of the next vms if a previous one failed
                    logging.error("Script will be aborted because {0} failed to shutdown".format(domainName))
                    exit (errorcodes.EXIT_SHUTDOWN_FAILED)

            else:
                logging.warning("Domain {0} is already shutdown".format(domainName))

    def restartAllShutdownDomains(self):

        domainNameList = map(lambda domain:self.libvirt.getDomainName(domain),self.domainsThatHaveBeenShutdown)
        logging.info("Now trying to restart the following domains: [ {0} ]".format(str(domainNameList)))

        while self.domainsThatHaveBeenShutdown:

            domain = self.domainsThatHaveBeenShutdown.pop()
            domainName = self.libvirt.getDomainName(domain)

            try:
                self.libvirt.startDomain(domain)

            except libvirtadapter.StartupError:
                logging.info("Failed to start {0}".format(domainName))
                self.restartAllShutdownDomains()
                # abort startup of the next vms if a previous one failed
                logging.error("Script will be aborted because {0} failed to start".format(domainName))
                exit(errorcodes.EXIT_STARTUP_FAILED)

