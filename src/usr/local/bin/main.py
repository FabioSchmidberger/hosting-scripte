#! /usr/bin/python

import sys
import os
import logging
import datetime

from shutdown import shutdownvms
from shutdown import argumentparser
from backupimages import backupimages

def main():
    argumentParser = argumentparser.ArgumentParser()
    argumentParser.parseArguments(sys.argv[1:])

    timestamp = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    logFileName = "backup-script." + timestamp + ".log"

    name = os.path.join(argumentParser.getTargetDirectory(), logFileName)

    logging.basicConfig(filename=name,
                        level=logging.INFO,
                        format = '%(asctime)s %(levelname)s {%(module)s} [%(funcName)s] %(message)s')

    logging.info("Started")

    vmList = argumentParser.getVmList()
    logging.info("The Vm list passed in is: [ {0} ]".format(str(vmList)))

    targetDirectoyBackup = argumentParser.getTargetDirectory()
    rebootAfterBackup = argumentParser.getRebootAfterBackup()

    shutdown = shutdownvms.ShutdownVms()
    shutdown.checkIfListOfDomainNamesExists(vmList)
    shutdown.shutdownDomains(vmList)

    backuper = backupimages.BackupImages(targetDirectoyBackup)
    backuper.backupDomains(vmList)

    if rebootAfterBackup == "1":
        shutdown.restartAllShutdownDomains()

    logging.info("Finished")

if __name__ == '__main__':

    main()

def getLogFileName(targetDirectoyLog):
    timestamp = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    logFileName = "backup-script." + timestamp + ".log"

    return os.path.join(targetDirectoyLog, logFileName)
