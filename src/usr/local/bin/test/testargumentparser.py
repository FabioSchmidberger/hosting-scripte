import unittest

from shutdown import argumentparser


class ArgumentParserTest(unittest.TestCase):


    def testGetVMListFromFile(self):
        argumentParser = argumentparser.ArgumentParser()
        argumentParser.vmListFilePath = "test/testVmList"

        vmList = argumentParser.getVmList()

        expectedList = ["vm1", "vm2", "reverseproxy"]

        self.assertListEqual(expectedList, vmList)
