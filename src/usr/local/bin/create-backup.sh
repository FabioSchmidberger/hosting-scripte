#!/bin/bash
READYFILE=/home/zusisys/READY

BACKUPDIR="/home/zusisys/current"

mkdir $BACKUPDIR
chown zusisys:zusisys $BACKUPDIR

# backup script

/usr/local/bin/main.py --vmfile /usr/local/bin/vmlist --configfile /usr/local/bin/configfile

compressBackupAndMarkReady ( ) {
    # cqow2 - Images komprimieren
    cd $BACKUPDIR && for file in $( ls *.qcow2 ); do /bin/gzip -9 $file; done

    touch "$READYFILE"
}

# error code handling
if [ $? -eq 0 ] # no error
then
  compressBackupAndMarkReady
elif [ $? -eq 2 ] # shutdown and backup successful, vm startup failed
then
  compressBackupAndMarkReady
fi

