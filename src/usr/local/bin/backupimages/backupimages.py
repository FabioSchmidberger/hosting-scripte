from xml.dom import minidom
import shutil
import logging
import os
import pwd
import grp

from shutdown import libvirtadapter
from shutdown import errorcodes

class BackupImages ():

    def __init__(self, targetDirectory):
        self.targetDirectory = self.validateTargetDirectory(targetDirectory)

    def backupDomains(self, domainNameList):
        for domainName in domainNameList:
            self.backupImageOfDomain(domainName)


    def backupImageOfDomain(self, domainName):
        fileListToBackup = self.getVmImagePathList(domainName)
        self.backupFilesFromList(fileListToBackup)


    # returns list of filepaths to all diskimages of the specified vm
    def getVmImagePathList(self, domainName):

        imageFileList = []

        # get libvirt xml config of the given domain
        libvirt = libvirtadapter.LibvirtAdapter()
        domain = libvirt.lookupByName(domainName)
        raw_xml = libvirt.getDomainRawXML(domain)

        # based on codeexample from
        # https://libvirt.org/docs/libvirt-appdev-guide-python/en-US/html/libvirt_application_development_guide_using_python-Guest_Domains-Device_Config-Disks.html
        xml = minidom.parseString(raw_xml)
        diskTypes = xml.getElementsByTagName('disk')

        for diskType in diskTypes:
            diskNodes = diskType.childNodes

            for diskNode in diskNodes:
                # find source node
                if diskNode.nodeName[0:1] != '#' and diskNode.nodeName == 'source':
                    # find file attribute
                    for attr in diskNode.attributes.keys():
                        if diskNode.attributes[attr].name == "file":
                            imageFileList.append(diskNode.attributes[attr].value)

        return imageFileList

    def backupFilesFromList(self, fileListToBackup):

        logging.info("Now trying to backup the following files: [ {0} ]".format(fileListToBackup))

        for fullSourceFilePath in fileListToBackup:
            self.backupFile(fullSourceFilePath)

    def backupFile(self, fullSourceFilePath):

        destinationFileName = self.generateFullDestinationFileName(fullSourceFilePath)


        try:
            logging.info("Starting to backup the file [ {0} ] to [ {1} ]".format(fullSourceFilePath, destinationFileName))
            shutil.copyfile(fullSourceFilePath, destinationFileName)
            logging.info("Backup of [ {0} ] to [ {1} ] finished".format(fullSourceFilePath, destinationFileName))
        except IOError, e:
            logging.error("Failed to Backup [ {0} ] to [ {1} ]".format(fullSourceFilePath, destinationFileName))
            exit(errorcodes.EXIT_BACKUP_FAILED)



    def generateFullDestinationFileName(self, fullSourceFilePath):
        #result: targetdir/filename (where filename is the name of the file saaved at fullSourceFilePath)
        return os.path.join(self.targetDirectory, os.path.basename(fullSourceFilePath))

    def validateTargetDirectory(self, targetDirectory):
        if not os.path.exists(targetDirectory):
            os.makedirs(targetDirectory)

        return targetDirectory